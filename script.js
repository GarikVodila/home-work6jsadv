/*
Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
*/

const findIpBtn = document.getElementById("find-ip-btn");
const addressByIp = document.getElementById("show-location-ip");

findIpBtn.addEventListener('click', () => sendRequestsAndRenderLocation());


async function sendRequestsAndRenderLocation() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const getIp = await response.json();

        const location = await fetch(`http://ip-api.com/json/${getIp.ip}?fields=status,message,continent,country,regionName,city,district,query`)
        const getLocation = await location.json();
        console.log(getLocation);
        showLocation(getLocation);

    } catch(error) {
        console.error(error);
    }
}

function showLocation (getLocation) {
    const { continent, country, regionName, city, district } = getLocation;

    addressByIp.innerHTML = "";

    const keysUa = ["Континент", "Країна", "Регіон", "Місто", "Район"];
    const values = Object.values({ continent, country, regionName, city, district });

    values.forEach((value, i) => {
         let locationList = document.createElement('ul');

        if (value !== "") {
            locationList.innerHTML += `<li>${keysUa[i]}: ${value}</li>`;
         }else{
            locationList.innerHTML += `<li>${keysUa[i]}: - sorry, no data available`;
         }
         
        addressByIp.append(locationList);
    });
   
}
